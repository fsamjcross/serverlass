import app from "./framework";

import UserController from './App/controllers/UserController';

app.use('/api/users',UserController.router());
