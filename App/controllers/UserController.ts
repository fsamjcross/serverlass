import Controller from "../../framework/Controller";
import UserModel  from '../models/User';
import IUser from "../interfaces/User";

export default class UserController extends Controller<IUser>{
    constructor() {
        super(UserModel);
    }
}